# Wisteria

Wisteria is an elegant Object-Oriented widget for modern JavaFX.

## Installation

Release x.x.x is the current latest release. This release is consider stable and worthy of the x.x tag. It is support Java Platform Module System (JPMS) with module name `com.swardana.wisteria`.

Available in the [Maven Central Repository][1]

### Prerequisites

You need to install:

- Minimum Java version 11 (The version provided by [Adoptium][2] is an excellent choice)
- and JavaFX 17+ (Yes, the required JavaFX version is 17, found [weird issues][3] with lower version).

Before using the Wisteria.

### As a Maven dependency

You can use [Maven's dependency management][1] to obtain the current latest release by adding the following configuration to the application's Project Object Model (POM) file

**Example - Maven**
```xml
<dependency>
  <groupId>com.swardana</groupId>
  <artifactId>wisteria</artifactId>
  <version>x.x.x</version>
</dependency>
```

### As a Gradle dependency

You can use [Gradle's dependency management][1] to obtain the current latest release by adding the following configuration to the application's `build.gradle` file

**Example - Gradle**
```groovy
implementation 'com.swardana:wisteria:x.x.x'
```

## Development

### Setup

After installing Java as directed in the [prerequisites][4] section, use the following command to clone the repository

```bash
$ git clone https://codeberg.org/swardana/wisteria.git
$ cd wisteria
```

You can now make changes in the repository.

### Building the Wisteria

To build the Wisteria, navigate into the `wisteria` directory and run the following command:

```bash
mvn clean install -T 1C
```

## Usage

**TBA**

## Support

Any donations to support the project are accepted via.

- [Paypal][5]
- [Buy Me a Coffee][6]

## Contributing

Help are welcome. For major changes, please send an email first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

This software is released under the 3-Clause BSD License. The complete license is located in [LICENSE][7]

[1]: https://search.maven.org/search?q=g:com.swardana%20AND%20a:wisteria
[2]: https://adoptium.net/temurin/archive/?version=11
[3]: https://codeberg.org/swardana/wisteria/wiki
[4]: README.md#prerequisites
[5]: https://www.paypal.me/sukmawardana/10
[6]: https://www.buymeacoffee.com/swardana
[7]: LICENSE
