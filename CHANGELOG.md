# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- A scrollable image widget with zoom and panning function. 
- A `menu` control, maintain and install a menu to the application.
- A `visual-proxy` API for the **presentation** layer.
