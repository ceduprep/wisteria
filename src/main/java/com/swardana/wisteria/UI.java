/*
 * Copyright (c) 2023, Sukma Wardana.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.swardana.wisteria;

import java.util.Optional;
import javafx.scene.Node;
import javafx.scene.Parent;

/**
 * A user interface visual proxy.
 * <p>
 *     Objects that implement a {@code UI} return
 *     proxies (<i>views</i>) for attributes
 *     of a <i>business</i> object when asked.
 * </p>
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
public interface UI {

    /**
     * Return a visual-proxy.
     * <p>
     *     Return the visual proxy for the attribute specified
     *     as an argument or {@code null} if the requested attribute
     *     isn't recognized.
     * </p>
     * <p>
     *     All objects that implement {@code UI} must support a null
     *     <i>attribute</i> argument which will return the <i>default</i>
     *     view, whatever that is.
     * </p>
     *
     * @param attribute the attribute that this proxy represents or
     *                  {@code null} for the default type.
     * @param isReadOnly if {@code true} then the proxy is non-editable.
     * @return the proxy, or {@code null} if the requested attribute
     * is not supported or recognized.
     */
    Optional<Node> visual(String attribute, boolean isReadOnly);

    /**
     * Return a non-editable visual-proxy.
     *
     * @param attribute the attribute that this proxy represents or
     *                  {@code null} for the default type.
     * @return the proxy, or {@code null} if the requested attribute
     * is not supported or recognized.
     */
    default Optional<Node> visual(String attribute) {
        return this.visual(attribute, true);
    }

    /**
     * Returns the default visual-proxy.
     * <p>
     *     Most the time, the default <i>view</i>
     *     is a {@link Parent}. This method will call
     *     {@link #visual(String, boolean)} with {@code null}
     *     attribute and in read-only mode.
     * </p>
     *
     * @return the proxy.
     * @throws java.util.NoSuchElementException if couldn't
     * the {@link Parent} or default <i>view</i>
     */
    default Parent visual() {
        return (Parent) this.visual(null, true).orElseThrow();
    }


}
