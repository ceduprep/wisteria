/*
 * Copyright (c) 2023, Sukma Wardana.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.swardana.wisteria.control;

import javafx.beans.NamedArg;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;

/**
 * An {@link ImageView} control.
 * <p>
 *     Resize the {@link ImageView} to the area available
 *     to the {@link Region}.
 * </p>
 * <p>
 *     WARNING: {@link ImageView} attached to this, it's not allowed
 *     to bind the {@link ImageView#fitWidthProperty()} and
 *     {@link ImageView#fitHeightProperty()} to other Pane.
 *     Otherwise it will throw an error!
 * </p>
 *
 * @see <a href="https://stackoverflow.com/a/15955755/5852226">StackOverflow</a>
 * @see <a href="https://bugs.openjdk.org/browse/JDK-8091216">JDK-8091216</a>
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
public final class ImageViewControl extends Region {

    private final ObjectProperty<ImageView> view;

    /**
     * Creates new ImageViewControl.
     */
    public ImageViewControl() {
        this(new ImageView());
    }

    /**
     * Creates new ImageViewControl.
     *
     * @param imageView the {@link ImageView} object.
     */
    public ImageViewControl(@NamedArg("imageView") final ImageView imageView) {
        this.view = new SimpleObjectProperty<>(this, "image", null);

        this.view.addListener(new ChangeListener<ImageView>() {
            @Override
            public void changed(
                final ObservableValue<? extends ImageView> observable,
                final ImageView oldView,
                final ImageView newView
            ) {
                if (oldView != null) {
                    getChildren().remove(oldView);
                }
                if (newView != null) {
                    getChildren().add(newView);
                }
            }
        });

        this.setView(imageView);
    }

    public ObjectProperty<ImageView> viewProperty() {
        return this.view;
    }

    public void setView(final ImageView img) {
        this.viewProperty().set(img);
    }

    public ImageView getView() {
        return this.viewProperty().get();
    }

    @Override
    protected void layoutChildren() {
        ImageView imgView = this.getView();
        if (imgView != null) {
            imgView.setFitWidth(getWidth());
            imgView.setFitHeight(getHeight());
            layoutInArea(
                imgView,
                0,
                0,
                getWidth(),
                getHeight(),
                0,
                HPos.CENTER,
                VPos.CENTER
            );
        }
        super.layoutChildren();
    }

}

