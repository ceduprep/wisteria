/*
 * Copyright (c) 2023, Sukma Wardana.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.swardana.wisteria;

import com.swardana.wisteria.tools.Assert;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.layout.Pane;

/**
 * A menu site.
 * <p>
 *     Help creating {@link MenuBar} and
 *     attached it to a {@link Pane}.
 * </p>
 * <p>
 *     Other objects in the system (which do not have to be
 *     visual objects) can negotiate with the {@link MenuSite}
 *     to have menu's placed on the site's {@link MenuBar}
 *     (or within submenus already found on the {@link MenuBar}.
 * </p>
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
public interface MenuSite {

    /**
     * Adds a menu to the {@link MenuSite} main
     * {@link MenuBar}.
     *
     * @param requester the object that creates the menu.
     *                  This does not have to be a visual
     *                  object.
     * @param menu the menu to be added.
     */
    void addMenu(Object requester, Menu menu);

    /**
     * Adds a line-item to a {@link Menu} already on the
     * {@link MenuSite}'s main {@link javafx.scene.control.MenuBar}.
     *
     * @param requester the object that creates the menu.
     *                  This does not have to be a visual
     *                  object.
     * @param item the line-item to be added. This could be
     *             a single {@link MenuItem} or an entire
     *             {@link Menu} that will become a submenus.
     * @param toThisMenu the label that identifies the menu to
     *                   which the item is to be added. The
     *                   {@code toThisMenu} {@link String} is
     *                   first compared against all the menu
     *                   labels (usually passed into the {@link Menu}
     *                   constructor or {@link Menu#setText(String)}).
     *                   <p>
     *                   The <i>"Help"</i> menu (identified by a label
     *                   having the value <i>"Help</i> or <i>"_Help"</i>).
     *                   No special methods are required to manipulate it.
     *                   </p>
     * @throws IllegalArgumentException if the menu specified in {@code toThisMenu}
     * can't be found.
     */
    void addLineItem(Object requester, MenuItem item, String toThisMenu);

    /**
     * Remove all {@link Menu}s and {@link MenuItem}'s that were added
     * by this requester.
     * <p>
     *     Note that the {@link MenuBar} is, itself, destroyed
     *     when it has no menus on it.
     * </p>
     *
     * @param requester the object that creates the menu.
     *                  This does not have to be a visual
     *                  object.
     */
    void removeMyMenus(Object requester);

    /**
     * An implementation support for {@link MenuSite}.
     * <p>
     *     This class provides support for implementing
     *     {@link MenuSite}. It has default versions of
     *     all menu-site methods, and also maintains
     *     the menu bar itself (which is installed
     *     on the {@link Pane}).
     * </p>
     * <p>
     *     Use it like this:
     *     <pre>{@code
     *     public class MenuWidget extends VBox implements MenuSite {
     *
     *         private final MenuSite.Implementation support;
     *
     *         public MenuWidget() {
     *             this.support = new MenuSite.Implementation(this);
     *         }
     *
     *         @Override
     *         public void addMenu(final Object requester, final Menu menu) {
     *             this.support.addMenu(requester, menu);
     *         }
     *
     *         @Override
     *         public void addLineItem(
     *             final Object requester,
     *             final MenuItem item,
     *             final String toThisMenu
     *         ) {
     *             this.support.addLineItem(requester, item, toThisMenu);
     *         }
     *
     *         @Override
     *         public final void removeMyMenus(final Object requester) {
     *             this.support.removeMyMenus(requester);
     *         }
     *     }
     *     }</pre>
     * </p>
     *
     * @author Sukma Wardana
     * @version 0.1
     * @since 0.1
     */
    final class Implementation implements MenuSite {

        /**
         * The {@code requesters} table keeps track of who requested
         * which menu items. It is indexed by requester and contains
         * a {@link List} of {@link Implementation.Item} objects
         * that identify all items added by that {@code requester}.
         */
        private final Map<Object, List<Item>> requesters;
        /**
         * The {@link #menuBarContents} contains references to the
         * various menus that comprise the {@link MenuBar}. This
         * kluge is necessary because JavaFX does not yet support
         * the notion of a <i>Help</i> menu, and it won't let you
         * insert menus anywhere other than the far right of the
         * {@link MenuBar}, where the <i>Help</i> menu should be.
         * Consequently, when a new menu is added to a {@link MenuBar},
         * you need to clear out the existing {@link MenuBar}
         * and rebuild it from scratch.
         */
        private final LinkedList<Item> menuBarContents;
        /**
         * The {@link Implementation} also encapsulates the {@link MenuBar}
         * itself, which it creates and installs on its {@link javafx.scene.layout.Pane}
         * (passed into the constructor) when the first item is added.
         */
        private final Pane pane;
        private MenuBar menuBar;

        /**
         * Creates new MenuSite.Implementation.
         *
         * @param pane the container for {@link MenuSite}.
         */
        public Implementation(final Pane pane) {
            this.requesters = new HashMap<>(0);
            this.menuBarContents = new LinkedList<>();
            this.menuBar = new MenuBar();
            this.pane = pane;
            /*
              Need to set the index 0 on {@link Pane#getChildren()}
              to avoid {@link IndexOutOfBoundsException} during
              {@link #updateMenuBar(MenuBar)} .
             */
            this.pane.getChildren().add(this.menuBar);
        }

        @Override
        public void addMenu(final Object requester, final Menu menu) {
            Item item = new Item(menu, this.menuBar, this.isHelpMenu(menu));
            this.requesterMenu(requester).add(item);
            item.attachMenuToContainer();
        }

        @Override
        public void addLineItem(
            final Object requester,
            final MenuItem item,
            final String toThisMenu
        ) {
            Assert.isTrue(requester != null, "Bad argument! The requester is null");
            Assert.isTrue(item != null, "Bad argument! The menu-item is null");
            Assert.isTrue(toThisMenu != null, "Bad argument! The toThisMenu is null");
            Menu found = null;
            /*
              Find the menu into which the line item should be inserted.
             */
            for (final Menu menu : this.menuBar.getMenus()) {
                final String text = menu.getText();
                final String mnemonicParsing = "_" + toThisMenu;
                if (toThisMenu.equalsIgnoreCase(text) || mnemonicParsing.equalsIgnoreCase(text)) {
                    found = menu;
                    break;
                }
            }
            /*
              If it can't find the menu, throw an exception.
             */
            if (found == null) {
                throw new IllegalArgumentException("Can't find menu (" + toThisMenu + ")!");
            }
            Item lineItem = new Item(item, found, false);
            this.requesterMenu(requester).add(lineItem);
            lineItem.attachMenuToContainer();
        }

        @Override
        public void removeMyMenus(final Object requester) {
            List<Item> menus = this.requesters.remove(requester);
            if (menus != null) {
                for (final Item item : menus) {
                    item.detachMenuFromContainer();
                }
            }
        }

        /**
         * This is a convenience method that manufactures {@link Menu}
         * with text labels.
         *
         * @param text the menu item name.
         * @return the newly create {@link Menu}.
         */
        public static Menu menu(final String text) {
            Menu menu = new Menu(text);
            menu.setMnemonicParsing(true);
            return menu;
        }

        /**
         * This is a convenience method that manufactures {@link MenuItem}
         * with text labels.
         *
         * @param text the menu item name.
         * @param action the menu action.
         * @return the newly create {@link MenuItem}.
         */
        public static MenuItem lineItem(final String text, final EventHandler<ActionEvent> action) {
            return lineItem(text, action, null);
        }

        /**
         * This is a convenience method that manufactures {@link MenuItem}
         * with text labels and accelerator.
         *
         * @param text the menu item name.
         * @param action the menu action.
         * @param accelerator the key code combination accelerator
         *                    for the {@link MenuItem}
         * @return the newly create {@link MenuItem}.
         */
        public static MenuItem lineItem(
            final String text,
            final EventHandler<ActionEvent> action,
            final KeyCodeCombination accelerator
        ) {
            MenuItem item = new MenuItem(text);
            item.setMnemonicParsing(true);
            if (Objects.nonNull(accelerator)) {
                item.setAccelerator(accelerator);
            }
            item.setOnAction(action);
            return item;
        }

        /**
         * Return a list of menu items associated with a given requester.
         * <p>
         *     A new (empty) {@link ArrayList} is created and returned
         *     if there are no menus associated with the requester at
         *     present.
         * </p>
         *
         * @param requester the object that creates the menu.
         * @return the list of menu items.
         */
        private List<Item> requesterMenu(final Object requester) {
            Assert.isTrue(requester != null, "Bad argument! The requester is null");
            Assert.isTrue(this.requesters != null, "No requesters yet!");
            List<Item> menus = this.requesters.get(requester);
            if (Objects.isNull(menus)) {
                menus = new ArrayList<>();
                this.requesters.put(requester, menus);
            }
            return menus;
        }

        /**
         * Return {@code true} if th menu passed as an argument
         * is the help menu.
         * <p>
         *     The name {@code help} is not case sensitive.
         * </p>
         *
         * @param menu the {@link Menu} that need to check.
         * @return {@code true} if menu text equals to {@code help}.
         */
        private boolean isHelpMenu(final Menu menu) {
            final boolean result;
            String text = menu.getText();
            result = "help".equalsIgnoreCase(text) || "_help".equalsIgnoreCase(text);
            return result;
        }

        /**
         * Replace the old {@link MenuBar} with the new one.
         *
         * @param regeneratedMenuBar the new {@link MenuBar}.
         */
        private void updateMenuBar(final MenuBar regeneratedMenuBar) {
            this.pane.getChildren().set(0, regeneratedMenuBar);
        }

        /**
         * An association between a menu or submenu.
         * <p>
         *     Makes the association between a line-item or
         *     submenu and the {@link MenuBar} or {@link Menu}
         *     that contains it.
         * </p>
         * <p>
         *     You can ask an {@link Item} to add or remove itself
         *     from its container. All the weirdness associated with
         *     help menus is handled here.
         * </p>
         *
         * @author Sukma Wardana
         * @version 1.0
         * @since 1.0
         */
        private final class Item {

            private final MenuItem lineItem;
            private final boolean isHelpMenu;
            private Object container;

            /**
             * Creates new Item.
             *
             * @param lineItem the line-item to be added.
             * @param container the container where line-item will be added.
             *                  Either {@link MenuBar} or {@link Menu}.
             * @param isHelpMenu telling whether the line-item is a help menu or not.
             */
            Item(final MenuItem lineItem, final Object container, final boolean isHelpMenu) {
                Assert.isTrue(container instanceof MenuBar || container instanceof Menu);
                this.lineItem = lineItem;
                this.container = container;
                this.isHelpMenu = isHelpMenu;
            }

            /**
             * Attach the line-item to its container.
             * <p>
             *     Attach a {@link MenuItem} to it's container
             *     (either a {@link MenuBar} or {@link Menu}).
             * </p>
             * <p>
             *     Items are added at the end of the {@code menuBarContents}
             *     list unless a help menu exists, in which case
             *     items are added at the penultimate position.
             * </p>
             */
            void attachMenuToContainer() {
                if (this.container instanceof Menu) {
                    /*
                      Either a line-item or submenus within {@link Menu}.
                     */
                    ((Menu) this.container).getItems().add(this.lineItem);
                } else {
                    /*
                      A menu within {@link MenuBar}.
                     */
                    if (menuBarContents.isEmpty()) {
                        menuBarContents.add(this);
                        ((MenuBar) container).getMenus().add((Menu) this.lineItem);
                    } else {
                        Item last = menuBarContents.getLast();
                        if (!last.isHelpMenu) {
                            menuBarContents.add(this);
                            ((MenuBar) container).getMenus().add((Menu) this.lineItem);
                        } else {
                            /*
                              Remove the help menu, add the new item,
                              then put the help menu back (following
                              the new item).
                             */
                            menuBarContents.removeLast();
                            menuBarContents.add(this);
                            menuBarContents.add(last);
                            container = regenerate();
                        }
                    }
                }
            }

            /**
             * Remove the current {@link MenuItem} from its
             * container (either a {@link MenuBar} or {@link Menu}).
             */
            void detachMenuFromContainer() {
                if (this.container instanceof Menu) {
                    ((Menu) this.container).getItems().remove(this.lineItem);
                } else {
                    ((MenuBar) this.container).getMenus().remove(this.lineItem);
                    menuBarContents.remove(this);
                    container = regenerate();
                }
            }

            /**
             * Replace the old menu bar wih a new one that reflects
             * the current state of the {@code menuBarContents} list.
             *
             * @return the newly generated {@link MenuBar}.
             */
            MenuBar regenerate() {
                menuBar = new MenuBar();
                for (final Item item : menuBarContents) {
                    menuBar.getMenus().add((Menu) item.lineItem);
                }
                updateMenuBar(menuBar);
                return menuBar;
            }

        }
    }

}
