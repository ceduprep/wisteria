/*
 * Copyright (c) 2023, Sukma Wardana.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.swardana.wisteria;

import com.swardana.wisteria.control.ImageViewControl;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.CacheHint;
import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Region;

/**
 * An scrollable image widget.
 * <p>
 *     A convenience class that creates {@link ImageView}
 *     and wraps it into a {@link ScrollPane} for you.
 *     Not only that, it's also have zoom and pan
 *     function.
 * </p>
 * <p>
 *     There is two option on how to updating the {@link Image}.
 *     Either pass the {@link ImageView} that you could control
 *     when create the {@link ScrollableImage} or relying the
 *     {@link #update(Image)} method.
 * </p>
 * <p>
 *     Example with {@link ImageView} that you could control:
 *     <pre>{@code
 *     ImageView independentImageView = new ImageView(new Image("source/to/image"));
 *     ScrollableImage scrollableImage = new ScrollableImage(independentImageView);
 *     }</pre>
 * </p>
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
public class ScrollableImage extends ScrollPane {

    private static final double SCALE_DELTA = 1.175;
    private static final double MIN_SCALE = 1;

    private final ImageView image;
    /**
     * Resize the {@link ImageView} it contains to the
     * area available to the {@link Region}.
     */
    private final ImageViewControl control;
    /**
     * The zoom-able contentpane of the {@link ScrollPane}.
     * <p>
     *     {@link ScrollPane} uses the unmodified layout bounds
     *     to determine the content size. This does not include
     *     the scaling transformation. To prevent the content
     *     from growing beyond the scrollable area,
     *     wrap the scaled node in a Group.
     * </p>
     */
    private final Group group;

    private final ObjectProperty<Point2D> mouseAnchor;
    private final ChangeListener<Bounds> onBoundsChange = new ChangeListener<Bounds>() {
        @Override
        public void changed(
            final ObservableValue<? extends Bounds> observable,
            final Bounds oldBounds,
            final Bounds currentBounds
        ) {
            control.setPrefSize(
                currentBounds.getWidth(),
                currentBounds.getHeight()
            );
        }
    };
    private final EventHandler<ScrollEvent> onScrolled = new EventHandler<ScrollEvent>() {
        @Override
        public void handle(final ScrollEvent event) {
            event.consume();
            if (event.getDeltaY() == 0) {
                return;
            }
            onViewZoom(
                event.getDeltaY(),
                image.getScaleX(),
                image.getScaleY()
            );
        }
    };
    private final EventHandler<MouseEvent> onMouseClicked = new EventHandler<MouseEvent>() {
        @Override
        public void handle(final MouseEvent event) {
            event.consume();
            if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {
                resetScale();
            }
        }
    };
    private final EventHandler<MouseEvent> onMousePressed = new EventHandler<MouseEvent>() {
        @Override
        public void handle(final MouseEvent event) {
            event.consume();
            mouseAnchor.set(
                new Point2D(
                    event.getX(),
                    event.getY()
                )
            );
        }
    };
    private final EventHandler<MouseEvent> onMouseDragged = new EventHandler<MouseEvent>() {
        @Override
        public void handle(final MouseEvent event) {
            event.consume();
            onMouseDragged(
                event.getX(),
                mouseAnchor.get().getX(),
                event.getY(),
                mouseAnchor.get().getY()
            );
        }
    };

    /**
     * Creates new ScrollableImage.
     */
    public ScrollableImage() {
        this(new ImageView());
    }

    /**
     * Creates new ScrollableImage.
     *
     * @param imgView the {@link ImageView} control.
     */
    public ScrollableImage(final ImageView imgView) {
        this.image = imgView;
        this.control = new ImageViewControl();
        this.group = new Group();
        this.mouseAnchor = new SimpleObjectProperty<>(this, "mouse", null);

        this.initGraphics();
        this.registerListeners();
    }

    /**
     * Update the {@link Image} on {@link ImageView}.
     *
     * @param img the new {@link Image} to replace
     *            the old {@link Image}.
     */
    public final void update(final Image img) {
        this.image.setImage(img);
    }

    /**
     * Requesting focus.
     * <p>
     *     Re-gain the focus onto this view.
     * </p>
     */
    public final void focus() {
        this.setFocusTraversable(true);
        this.requestFocus();
    }

    /**
     * Change the scale of picture.
     *
     * @param horizontal the scale factor by which coordinates along the X axis.
     * @param vertical the scale factor by which coordinates along the Y axis.
     */
    public final void rescale(final double horizontal, final double vertical) {
        this.image.setScaleX(horizontal);
        this.image.setScaleY(vertical);
    }

    /**
     * Reset the picture scale.
     */
    public final void resetScale() {
        this.rescale(1, 1);
    }

    /**
     * Handling the zoom operation.
     *
     * @param delta the vertical scroll amount.
     * @param horizontal the scale factor by which coordinates along the X axis.
     * @param vertical the scale factor by which coordinates along the Y axis.
     */
    final void onViewZoom(
        final double delta,
        final double horizontal,
        final double vertical
    ) {
        double scale = this.scaleFactor(delta);

        /*
          Amount of scrolling in each direction in scrollContent coordinate
          units.
         */
        double scrollXOffset = this.horizontalScrollOffset();
        double scrollYOffset = this.verticalScrollOffset();
        double maxHorizontal = Math.max(horizontal * scale, MIN_SCALE);
        double maxVertical = Math.max(vertical * scale, MIN_SCALE);

        this.rescale(maxHorizontal, maxVertical);
        this.repositionScroller(scale, scrollXOffset, scrollYOffset);
    }

    /**
     * Reposition the viewport of Scroller.
     *
     * @param scaleFactor the scale factor.
     * @param scrollXOffset the horizontal offset position.
     * @param scrollYOffset the vertical offset position.
     */
    final void repositionScroller(
        final double scaleFactor,
        final double scrollXOffset,
        final double scrollYOffset
    ) {
        double extraWidth = this.group.getLayoutBounds().getWidth()
                                - this.getViewportBounds().getWidth();

        if (extraWidth > 0) {
            double halfWidth = this.getViewportBounds().getWidth() / 2;

            double newScrollXOffset = (scaleFactor - 1)
                                          *  halfWidth + scaleFactor * scrollXOffset;

            this.setHvalue(
                this.getHmin() + newScrollXOffset
                                     * (this.getHmax() - this.getHmin()) / extraWidth
            );
        } else {
            this.setHvalue(this.getHmin());
        }

        double extraHeight = this.group.getLayoutBounds().getHeight()
                                 - this.getViewportBounds().getHeight();

        if (extraHeight > 0) {
            double halfHeight = this.getViewportBounds().getHeight() / 2;

            double newScrollYOffset = (scaleFactor - 1)
                                          * halfHeight + scaleFactor * scrollYOffset;

            this.setVvalue(
                this.getVmin() + newScrollYOffset
                                     * (this.getVmax() - this.getVmin()) / extraHeight
            );
        } else {
            this.setHvalue(this.getHmin());
        }
    }

    /**
     * Figure the horizontal scroll offset position.
     *
     * @return the horizontal scroll offset position.
     */
    final double horizontalScrollOffset() {
        double extraWidth = this.group.getLayoutBounds().getWidth()
                                - this.getViewportBounds().getWidth();

        double hScrollProportion = (this.getHvalue() - this.getHmin())
                                       / (this.getHmax() - this.getHmin());

        return hScrollProportion * Math.max(0, extraWidth);
    }

    /**
     * Figure the vertical scroll offset position.
     *
     * @return the vertical scroll offset position.
     */
    final double verticalScrollOffset() {
        double extraHeight = this.group.getLayoutBounds().getHeight()
                                 - this.getViewportBounds().getHeight();

        double vScrollProportion = (this.getVvalue() - this.getVmin())
                                       / (this.getVmax() - this.getVmin());

        return vScrollProportion * Math.max(0, extraHeight);
    }

    /**
     * Handling the panning operation.
     *
     * @param clickedX the horizontal point when mouse clicked.
     * @param draggedX the horizontal point when mouse stopped after drag.
     * @param clickedY the vertical point when mouse clicked.
     * @param draggedY the vertical point when mouse stopped after drag.
     */
    @SuppressWarnings("checkstyle:ParameterNumber")
    final void onMouseDragged(
        final double clickedX,
        final double draggedX,
        final double clickedY,
        final double draggedY
    ) {
        double deltaX = clickedX - draggedX;
        double deltaY = clickedY - draggedY;

        this.horizontalMove(deltaX);
        this.verticalMove(deltaY);
    }

    /**
     * Move the node horizontally based on delta x.
     *
     * @param deltaX the delta x.
     */
    final void horizontalMove(final double deltaX) {
        double extraWidth = this.group.getLayoutBounds().getWidth()
                                - this.getViewportBounds().getWidth();

        double deltaH = deltaX * (this.getHmax() - this.getHmin()) / extraWidth;

        double desiredH = this.getHvalue() - deltaH;

        this.setHvalue(Math.max(0, Math.min(this.getHmax(), desiredH)));
    }

    /**
     * Move the node vertically based on delta y.
     *
     * @param deltaY the delta y.
     */
    final void verticalMove(final double deltaY) {
        double extraHeight = this.group.getLayoutBounds().getHeight()
                                 - this.getViewportBounds().getHeight();

        double deltaV = deltaY * (this.getHmax() - this.getHmin()) / extraHeight;

        double desiredV = this.getVvalue() - deltaV;

        this.setVvalue(Math.max(0, Math.min(this.getVmax(), desiredV)));
    }

    private double scaleFactor(final double delta) {
        final double result;
        if (delta > 0) {
            result = SCALE_DELTA;
        } else {
            result = MIN_SCALE / SCALE_DELTA;
        }
        return result;
    }

    private void initGraphics() {
        this.image.setCache(true);
        this.image.setCacheHint(CacheHint.SPEED);
        this.image.setPreserveRatio(true);
        this.image.setSmooth(true);
        this.control.setView(this.image);
        this.group.setCache(true);
        this.group.setCacheHint(CacheHint.SPEED);
        this.group.getChildren().add(this.control);
        this.setContent(this.group);
    }

    private void registerListeners() {
        this.control.setOnScroll(this.onScrolled);
        this.viewportBoundsProperty().addListener(this.onBoundsChange);
        this.setOnMouseClicked(this.onMouseClicked);
        this.group.setOnMousePressed(onMousePressed);
        this.group.setOnMouseDragged(onMouseDragged);
    }

}
