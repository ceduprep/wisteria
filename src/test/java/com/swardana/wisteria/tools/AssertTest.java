/*
 * Copyright (c) 2023, Sukma Wardana.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.swardana.wisteria.tools;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Unit test for {@link Assert}.
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
class AssertTest {


    @Nested
    class IsTrue {

        @Test
        void doNothingWhenTheConditionIsTrue() {
            assertDoesNotThrow(() -> Assert.isTrue(true));
        }

        @Test
        void exceptionThrownWhenTheConditionIsFalse() {
            assertThrowsExactly(Assert.Failed.class, () -> Assert.isTrue(false));
        }

        @Test
        void exceptionThrownWithMessageWhenTheConditionIsFalse() {
            assertThatThrownBy(() -> Assert.isTrue(false, "Bad condition!"))
                .isInstanceOf(Assert.Failed.class)
                .hasMessage("Bad condition!");
        }
    }

    @Nested
    class IsFalse {

        @Test
        void doNothingWhenTheConditionIsFalse() {
            assertDoesNotThrow(() -> Assert.isFalse(false));
        }

        @Test
        void exceptionThrownWhenTheConditionIsTrue() {
            assertThrowsExactly(Assert.Failed.class, () -> Assert.isFalse(true));
        }

        @Test
        void exceptionThrownWithMessageWhenTheConditionIsTrue() {
            assertThatThrownBy(() -> Assert.isFalse(true, "Bad condition!"))
                .isInstanceOf(Assert.Failed.class)
                .hasMessage("Bad condition!");
        }
    }

}

